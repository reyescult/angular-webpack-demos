# Angular Webpack Demos #

This is an Angular with Webpack demonstration project.

### What is this repository for? ###

* This reposity serves an appication to demonstrate custom Angular components, directives, services, etc.

### Feature Demos

* Browser Info Service (BrowserInfoService) - Returns the current browser's name, version and name-version. It is added to the providers in app.module.ts and is used in app.component.ts to append a browser 'name-version' (Ex. chrome-55) class to the html element.
* Browser Info Component to demonstrate conditional HTML content (*ngIf) using the Browser Info Service.
* App Menu Component for creating drop-down menus for site navigation, etc.
* Tooltip Component for creating contextual tooltips. * This component extends the app menu component (required).
* Numeric Input Directive to restrict input to characters 0-9 via keyboard and paste.

### How do I get set up? ###

* `git clone https://reyescult@bitbucket.org/reyescult/angular-webpack-demos.git`
* `cd angular-webpack-demos`
* `npm install`

### NPM scripts ###

* `npm start` to spin up the Webpack development server
* `npm run build` to generate the production build
* `npm run clean` to delete the build folder

### Who do I talk to? ###

* Your Mom