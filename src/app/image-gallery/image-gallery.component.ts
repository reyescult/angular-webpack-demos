import { Component, Input, AfterViewInit, ViewChild, ViewChildren, QueryList } from '@angular/core';

@Component({
  selector: 'image-gallery',
  templateUrl: './image-gallery.component.html',
  styleUrls: ['./image-gallery.component.scss']
})
export class ImageGalleryComponent implements AfterViewInit {
  @Input() images: Array<{}>;
  @Input() aspectRatio: string;
  @Input() maxWidth: number;
  @Input() hideControls: boolean;
  @Input() hidePagination: boolean;
  @Input() showThumbnails: boolean;
  @Input() thumbnailsPerPage: number = 4;
  @Input() hidePageCount: boolean;

  @ViewChild('imageGalleryContainer') imageGalleryContainer: any;
  @ViewChild('thumbSlide') thumbSlide: any;
  @ViewChildren('thumbLink') thumbLinks: QueryList<any>;

  public currentImage: number = 0;
  public thumbPageNumber: number = 1;
  public thumbPageCount: number;
  
  private thumbSlidePosition: number = 0;
  private thumbSlidePositionEnd: number;

  ngAfterViewInit() {
    setTimeout(() => {
      this.aspectRatio = (this.aspectRatio === 'hd') ? 'hd' : 'sd';
      this.thumbPageCount = Math.ceil(this.thumbLinks.length / this.thumbnailsPerPage);
    }, 0);
    this.imageGalleryContainer.nativeElement.style.maxWidth = this.maxWidth + 'px';
    this.thumbSlidePositionEnd = Math.ceil(this.thumbLinks.length / this.thumbnailsPerPage) * 100 - 100;
		this.thumbLinks.forEach(thumbLink => {
			thumbLink.nativeElement.style.width = (100 / this.thumbnailsPerPage) + '%';
      thumbLink.nativeElement.style.paddingTop = ((this.aspectRatio === 'hd') ? (56.25 / this.thumbnailsPerPage) : (75 / this.thumbnailsPerPage)) + '%';
		});
  }

  showPrev() {
    this.currentImage = (this.currentImage > 0) ? (this.currentImage - 1) : (this.images.length - 1);
  }

  showNext() {
    this.currentImage = (this.currentImage < this.images.length - 1) ? (this.currentImage + 1) : 0;
  }

  showImage(i: number) {
    this.currentImage = i;
  }

  thumbsPrev() {
    if (this.thumbSlidePosition !== 0) {
      this.thumbSlide.nativeElement.style.left = (this.thumbSlidePosition += 100) + '%';
      this.thumbPageNumber = (this.thumbPageNumber -= 1);
    }
  }

  thumbsNext() {
    if (this.thumbSlide.nativeElement.style.left !== '-' + this.thumbSlidePositionEnd + '%') {
      this.thumbSlide.nativeElement.style.left = (this.thumbSlidePosition -= 100) + '%';
      this.thumbPageNumber = (this.thumbPageNumber += 1);
    }
  }
}