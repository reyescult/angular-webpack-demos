import { Component } from '@angular/core';

@Component({
  selector: 'image-gallery-demo',
  template: `
    <h2>SD Image Gallery</h2>
    <image-gallery [maxWidth]="640" [images]="images.sd" [showThumbnails]="true" [thumbnailsPerPage]="5"></image-gallery>
    <h2>HD Image Gallery</h2>
    <image-gallery [maxWidth]="640" [images]="images.hd" [showThumbnails]="true" [aspectRatio]="'hd'" [hidePageCount]="true"></image-gallery>
  `,
  styles: ['h2 {margin-bottom: 20px; text-align: center;} h2:nth-of-type(2) {margin: 20px 0;}']
})
export class ImageGalleryDemoComponent {
  images: {} = {
    "hd" : [
        {
            "src" : "src/app/image-gallery/assets/hd/hd-01.png",
            "alt" : "HD Image 01",
            "des" : "High definition example image."
        },
        {
            "src" : "src/app/image-gallery/assets/hd/hd-02.png",
            "alt" : "HD Image 02",
            "des" : "High definition example image."
        },
        {
            "src" : "src/app/image-gallery/assets/hd/hd-03.png",
            "alt" : "HD Image 03",
            "des" : "High definition example image."
        },
        {
            "src" : "src/app/image-gallery/assets/hd/hd-04.png",
            "alt" : "HD Image 04",
            "des" : "High definition example image."
        },
        {
            "src" : "src/app/image-gallery/assets/hd/hd-05.png",
            "alt" : "HD Image 05",
            "des" : "High definition example image."
        },
        {
            "src" : "src/app/image-gallery/assets/hd/hd-06.png",
            "alt" : "HD Image 06",
            "des" : "High definition example image."
        },
        {
            "src" : "src/app/image-gallery/assets/hd/hd-07.png",
            "alt" : "HD Image 07",
            "des" : "High definition example image."
        },
        {
            "src" : "src/app/image-gallery/assets/hd/hd-08.png",
            "alt" : "HD Image 08",
            "des" : "High definition example image."
        }
    ],
    "sd" : [
        {
            "src" : "src/app/image-gallery/assets/sd/sd-01.png",
            "alt" : "SD Image 01",
            "des" : "Standard definition example image."
        },
        {
            "src" : "src/app/image-gallery/assets/sd/sd-02.png",
            "alt" : "SD Image 02",
            "des" : "Standard definition example image."
        },
        {
            "src" : "src/app/image-gallery/assets/sd/sd-03.png",
            "alt" : "SD Image 03",
            "des" : "Standard definition example image."
        },
        {
            "src" : "src/app/image-gallery/assets/sd/sd-04.png",
            "alt" : "SD Image 04",
            "des" : "Standard definition example image."
        },
        {
            "src" : "src/app/image-gallery/assets/sd/sd-05.png",
            "alt" : "SD Image 05",
            "des" : "Standard definition example image."
        },
        {
            "src" : "src/app/image-gallery/assets/sd/sd-06.png",
            "alt" : "SD Image 06",
            "des" : "Standard definition example image."
        },
        {
            "src" : "src/app/image-gallery/assets/sd/sd-07.png",
            "alt" : "SD Image 07",
            "des" : "Standard definition example image."
        },
        {
            "src" : "src/app/image-gallery/assets/sd/sd-08.png",
            "alt" : "SD Image 08",
            "des" : "Standard definition example image."
        }
    ]
  };
}