import { Component } from '@angular/core';

@Component({
  selector: 'on-keycode',
  templateUrl: './on-keycode.component.html',
  styleUrls: ['./on-keycode.component.scss']
})
export class OnKeycodeComponent {
  public disabled: boolean = false;
  public confirmation: string;
  public eKeyCode: number = 13;
  public eKey: string = 'Enter';
  
  setKeycode(e: KeyboardEvent) {
    this.eKeyCode = e.keyCode;
    this.eKey = e.key;
  }

  onKeycodeMatches(args: string) {
    this.confirmation = args;
    alert(this.confirmation);
  }
}