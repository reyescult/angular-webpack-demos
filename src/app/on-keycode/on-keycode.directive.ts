import { Directive, HostListener, Input } from '@angular/core';

@Directive({
  selector: '[onKeycode]'
})
export class OnKeycodeDirective {
  @Input() onKeycodeWhich: number;
  @Input() onKeycodeMatch: any;
  @Input() onKeycodeArguments: any;
  @Input() onKeycodeDisabled: any;

  @HostListener('document:keydown', ['$event'])
  onkeydown(e: KeyboardEvent) {
    let disabled = (typeof this.onKeycodeDisabled === 'function' ? this.onKeycodeDisabled() : this.onKeycodeDisabled);
    if (e.keyCode == this.onKeycodeWhich && !disabled) {
      this.onKeycodeMatch(this.onKeycodeArguments);
    }
  }
}