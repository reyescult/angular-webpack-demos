import { Component } from '@angular/core';
import { BrowserInfoService } from './browser-info/browser-info.service';

import '../../public/scss/styles.scss';

@Component({
  selector: 'my-app',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  constructor(private BrowserInfoService: BrowserInfoService) {

  }
  ngOnInit() {
    document.documentElement.classList.add(this.BrowserInfoService.browserNameVersion().toLowerCase());
  }
}