import { Component, Input, HostListener, ElementRef } from '@angular/core';
import { Router, NavigationStart } from '@angular/router';

@Component({
  selector: 'app-menu',
  templateUrl: './app-menu.component.html',
  styleUrls: ['./app-menu.component.scss']
})
export class AppMenuComponent {
  @Input() menuName: string;

  public showMenu: boolean = false;

  constructor(private el: ElementRef, private router: Router) {
    router.events.subscribe((e) => {
      if(e instanceof NavigationStart) {
        this.close();
      }
    });
  }

  @HostListener('document:keyup', ['$event'])
  private onKeyUp(e: KeyboardEvent) {
    if (e.keyCode === 27) {
      this.close();
    }
  }

  @HostListener('document:click', ['$event'])
  private onClick(e: any) {
    if (!this.el.nativeElement.contains(e.target)) {
      this.close();
    }
  }

  onBtnClick() {
    this.showMenu = !this.showMenu;
  }

  onLinkClick() {
    this.close();
  }

  private close() {
    this.showMenu = false;
  }
}