import { Component } from '@angular/core';

@Component({
  selector: 'uppercase-input',
  templateUrl: './uppercase-input.component.html',
  styleUrls: ['./uppercase-input.component.scss']
})
export class UppercaseInputComponent {
  public userName: string = 'THOMAS';
  public focusName: string;
  public blurName: string;

  private currentName: string = this.userName;

  onFocus(input: number) {
    this.focusName = this.userName;
    this.userName = '';
  }

  onBlur(input: number) {
    this.userName = this.userName ? this.userName : this.currentName;
    this.blurName = this.userName;
  }
}