import { Directive, HostListener, Output, EventEmitter } from '@angular/core';

@Directive({
  selector: '[uppercaseInput]'
})
export class UppercaseInputDirective {
  @Output() ngModelChange: EventEmitter<any> = new EventEmitter();

  @HostListener('input', ['$event'])
  oninput(e: any) {
    let element = e.target;
    let position = element.selectionStart;
    let value = e.target.value.toUpperCase();
    let changed = element.value.length !== value.length;
        position -= changed ? 1 : 0;

    this.ngModelChange.emit(element.value = value);
    
    setTimeout(() => {
      element.selectionStart = position;
      element.selectionEnd = position;
    });
  }
}