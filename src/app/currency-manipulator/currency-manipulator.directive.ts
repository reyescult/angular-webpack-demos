import { Directive, ElementRef, HostListener } from '@angular/core';

@Directive({
  selector: '[currencyManipulator]'
})
export class CurrencyManipulatorDirective {
  constructor(private el: ElementRef) { }

  @HostListener('window:load', ['$event'])
  onload(e: any) {
    this.setCurrency(this.el.nativeElement);
  }

  @HostListener('focus', ['$event'])
  onfocus(e: any) {
    this.stripCurrency(e.target);
  }

  @HostListener('blur', ['$event'])
  onblur(e: any) {
    this.setCurrency(e.target);
  }
  
  private stripCurrency(item: any) {
    var parsed = item.value.split('.')[0].replace(/\D/g, '');
    item.value = parsed > 0 ? parsed : '';
    item.setSelectionRange(0, item.value.length);
  }
  
  private setCurrency(item: any) {
    var number = item.value ? parseFloat(item.value) : 0;
    var parsed = number.toLocaleString(undefined, {
      minimumFractionDigits: 2,
      maximumFractionDigits: 2
    });
    item.value = '$' + parsed;
  }
}