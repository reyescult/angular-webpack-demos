import { Component } from '@angular/core';

@Component({
  selector: 'tooltip-demo',
  template: `
    <h2>Tooltip Demo</h2>
    <app-tooltip menuName="Learn More">
      <p class="app-tt-target">Lorem ipsum dolor sit amet.</p>
      <p class="app-tt-content">Proin sollicitudin tellus vitae felis hendrerit convallis quis id dolor.</p>
    </app-tooltip>
  `,
  styles: [':host {display: block; max-width: 960px; margin: 0 auto;}']
})
export class TooltipDemoComponent {

}