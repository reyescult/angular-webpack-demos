import { Component } from '@angular/core';

import { AppMenuComponent } from '../app-menu/app-menu.component';

@Component({
  selector: 'app-tooltip',
  templateUrl: './app-tooltip.component.html',
  styleUrls: ['./app-tooltip.component.scss']
})
export class AppTooltipComponent extends AppMenuComponent {

}