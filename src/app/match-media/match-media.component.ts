import { Component, ChangeDetectorRef, OnInit, OnDestroy } from '@angular/core';
import { Subscription }   from 'rxjs/Subscription';

import { MatchMediaService } from './match-media.service';

@Component({
  selector: 'match-media',
  templateUrl: './match-media.component.html',
  styleUrls: ['./match-media.component.scss']
})
export class MatchMediaComponent implements OnInit, OnDestroy {
  public mediaType: string = '<no media announced>';
  
  private subscription: Subscription;

  constructor(private changeDetectorRef: ChangeDetectorRef, private matchMediaService: MatchMediaService) {
    this.subscription = matchMediaService.mediaStream$.subscribe(
      data => {
        this.mediaType = data;
        this.changeDetectorRef.detectChanges();
      });
  }

  ngOnInit() {
    this.matchMediaService.startMatchMedia({desktop: 1080, tablet: 768});
  }

  ngOnDestroy() {
    this.matchMediaService.stopMatcheMedia();
    this.subscription.unsubscribe();
  }
}