import { Directive, Input, TemplateRef, ViewContainerRef, OnDestroy } from '@angular/core';

@Directive({ selector: '[matchMedia]'})
export class MatchMediaDirective implements OnDestroy {
  private hasView: boolean = false;
  private mqList: MediaQueryList;
  private mqListener: MediaQueryListListener = this.setMedia.bind(this);

  constructor(
    private templateRef: TemplateRef<any>,
    private viewContainer: ViewContainerRef) { }

  @Input() set matchMedia(mq: string) {
    this.mqList = window.matchMedia(mq);
    this.setMedia(this.mqList);
    this.mqList.addListener(this.mqListener);
  }

  ngOnDestroy() {
    this.mqList.removeListener(this.mqListener);
  }

  private setMedia(mql: MediaQueryList) {
    if (mql.matches && !this.hasView) {
      this.viewContainer.createEmbeddedView(this.templateRef);
      this.hasView = true;
    } else if (!mql.matches && this.hasView) {
      this.viewContainer.clear();
      this.hasView = false;
    }
  }
}