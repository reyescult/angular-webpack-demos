import { Injectable } from '@angular/core';
import { Subject }    from 'rxjs/Subject';

@Injectable()

export class MatchMediaService {
  private mqObj: any = {};
  private mqList: MediaQueryList[] = [];
  private mqListener: MediaQueryListListener = this.setMedia.bind(this);
  private mqMatch: string = '';
  
  private mediaSource = new Subject<string>();
  mediaStream$ = this.mediaSource.asObservable();

  startMatchMedia(obj: any) {
    this.mqObj = obj;
    this.mqList = [
      window.matchMedia('(min-width: ' + this.mqObj.desktop + 'px)'),
      window.matchMedia('(min-width: ' + this.mqObj.tablet + 'px)')
    ];
    for (let i of this.mqList) {
      this.setMedia(i);
      i.addListener(this.mqListener); // add listener on state changes
    }
  }
  
  stopMatcheMedia() {
    for (let i of this.mqList) {
      i.removeListener(this.mqListener); // remove listener on state changes
    }
    this.remClass();
  }

  private setMedia(mql: any): void {
    this.mqMatch = this.mqList[0].matches ? 'desktop' : this.mqList[1].matches ? 'tablet' : 'mobile';
    this.setClass();
    this.mediaSource.next(this.mqMatch);
  }
  
  private setClass(): void {
    this.remClass();
    document.documentElement.classList.add(this.mqMatch + '-media');
  }

  private remClass(): void {
    document.documentElement.classList.remove('desktop-media', 'tablet-media', 'mobile-media');
  }
}