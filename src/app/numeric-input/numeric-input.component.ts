import { Component } from '@angular/core';

@Component({
  selector: 'numeric-input',
  templateUrl: './numeric-input.component.html',
  styleUrls: ['./numeric-input.component.scss']
})
export class NumericInputComponent {
  public inputVal: number = 24.65;
  public focusVal: number;
  public blurVal: number;

  onFocus(input: number) {
    this.focusVal = this.inputVal;
  }

  onBlur(input: number) {
    this.blurVal = this.inputVal;
  }
}