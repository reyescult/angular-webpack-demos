import { Directive, HostListener, Input, Output, EventEmitter } from '@angular/core';

@Directive({
  selector: '[numericInput]'
})
export class NumericInputDirective {
  @Input() numericInput: string;
  @Output() ngModelChange: EventEmitter<any> = new EventEmitter();

  @HostListener('keypress', ['$event']) 
  onkeypress(e: any) {
    let code = e.keyCode || e.which;
    let decimal = this.numericInput === 'decimal' && code === 46; // allow decimal key
    let numbers = code >= 48 && code <= 57; // number keys
    let actions = code === 8 || code === 9 || code === 13; // backspace key, tab key, enter key
    let arrows = code >= 37 && code <= 40; // arrow keys

    if (decimal && !e.target.value.includes('.')) {
      return;
    }
    
    return (numbers || actions || arrows) ? true : false;
  }

  @HostListener('paste', ['$event'])
  onpaste(e: any) {
    e.preventDefault();
    this.ngModelChange.emit(e.target.value = e.clipboardData.getData('text').replace(/\D/g, ''));
  }
}