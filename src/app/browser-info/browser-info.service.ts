import { Injectable } from '@angular/core';

@Injectable()

export class BrowserInfoService {

  browserName() {
    return this.browserInfo().name;
  }

  browserVersion() {
    return this.browserInfo().version;
  }

  browserNameVersion() {
    return this.browserInfo().name + '-' + this.browserInfo().version;
  }

  private browserInfo() {
    let ua = navigator.userAgent,
        match = ua.match(/(chrome|firefox|safari|trident|crios)\/(\d+)/i) || [];
    if (match[1] === 'Chrome') {
      let edgeOpr = ua.match(/(edge|opr)\/(\d+)/i);
      if (edgeOpr) {
        return {
          name: edgeOpr[1] === 'Edge' ? 'Edge' : 'Opera',
          version: edgeOpr[2]
        };
      }
    }
    if (match[1] === 'Safari') {
      let sVersion = ua.match(/version\/(\d+)/i);
      return {
        name: match[1],
        version: sVersion[1]
      };
    }
    if (match[1] === 'Trident') {
      let msie = ua.match(/msie\s(\d+)/i),
          ieVersion = msie ? msie[1] : 11;
      return {
        name: 'IE',
        version: ieVersion
      };
    }
    if (match[1] === 'CriOS') {
      return {
        name: 'Chrome',
        version: match[2]
      };
    }
    return {
      name: match[1],
      version: match[2]
    };
  }

}