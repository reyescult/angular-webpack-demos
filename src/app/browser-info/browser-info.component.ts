import { Component } from '@angular/core';
import { BrowserInfoService } from './browser-info.service';

@Component({
  selector: 'browser-info',
  templateUrl: './browser-info.component.html',
  styleUrls: ['./browser-info.component.scss']
})
export class BrowserInfoComponent {
  browserName: string;
  browserMessage: string;
  constructor(private BrowserInfoService: BrowserInfoService) {

  }
  ngOnInit() {
    this.browserName = this.BrowserInfoService.browserName();
    this.browserMessage = ('Your browser is ' + this.BrowserInfoService.browserName() + ' ' + this.BrowserInfoService.browserVersion());
  }
}