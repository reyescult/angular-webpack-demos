import { Directive, HostListener, Input, Output, EventEmitter } from '@angular/core';

@Directive({
  selector: '[alphanumericInput]'
})
export class AlphanumericInputDirective {
  @Input() alphanumericInput: string;
  @Output() ngModelChange: EventEmitter<any> = new EventEmitter();

  @HostListener('input', ['$event'])
  oninput(e: any) {
    let element = e.target;
    let position = element.selectionStart;
    let value = element.value.replace(/[^a-zA-Z0-9]/g, '');
    let changed = element.value.length !== value.length;
    let uppercase = this.alphanumericInput === 'uppercase';
        position -= changed ? 1 : 0;
        value = uppercase ? value.toUpperCase() : value;

    this.ngModelChange.emit(element.value = value);
    
    setTimeout(() => {
      element.selectionStart = position;
      element.selectionEnd = position;
    });
  }
}