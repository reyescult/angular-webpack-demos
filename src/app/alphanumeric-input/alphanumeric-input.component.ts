import { Component } from '@angular/core';

@Component({
  selector: 'alphanumeric-input',
  templateUrl: './alphanumeric-input.component.html',
  styleUrls: ['./alphanumeric-input.component.scss']
})
export class AlphanumericInputComponent {
  public inputVal: string = '';
  public focusVal: string;
  public blurVal: string;

  onFocus(input: string) {
    this.focusVal = this.inputVal;
  }

  onBlur(input: string) {
    this.blurVal = this.inputVal;
  }
}