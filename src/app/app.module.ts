import { NgModule } from '@angular/core';
import { BrowserModule }  from '@angular/platform-browser';
import { FormsModule }   from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';

import { AppComponent } from './app.component';
import { AppHeaderComponent } from './app-header/app-header.component';
import { AppHomeComponent } from './app-home/app-home.component';
import { AppMenuComponent } from './app-menu/app-menu.component';
import { BackToTopComponent } from './back-to-top/back-to-top.component';
import { BrowserInfoService } from './browser-info/browser-info.service';
import { BrowserInfoComponent } from './browser-info/browser-info.component';
import { AppTooltipComponent } from './app-tooltip/app-tooltip.component';
import { TooltipDemoComponent } from './app-tooltip/tooltip-demo.component';
import { NumericInputComponent } from './numeric-input/numeric-input.component';
import { NumericInputDirective } from './numeric-input/numeric-input.directive';
import { OnKeycodeComponent } from './on-keycode/on-keycode.component';
import { OnKeycodeDirective } from './on-keycode/on-keycode.directive';
import { ScrollToComponent } from './scroll-to/scroll-to.component';
import { ScrollToDirective } from './scroll-to/scroll-to.directive';
import { UppercaseInputComponent } from './uppercase-input/uppercase-input.component';
import { UppercaseInputDirective } from './uppercase-input/uppercase-input.directive';
import { AlphanumericInputComponent } from './alphanumeric-input/alphanumeric-input.component';
import { AlphanumericInputDirective } from './alphanumeric-input/alphanumeric-input.directive';
import { CurrencyManipulatorComponent } from './currency-manipulator/currency-manipulator.component';
import { CurrencyManipulatorDirective } from './currency-manipulator/currency-manipulator.directive';
import { DynamicFormComponent } from './dynamic-form/dynamic-form.component';
import { DynamicInputComponent } from './dynamic-form/dynamic-input/dynamic-input.component';
import { ImageGalleryDemoComponent } from './image-gallery/image-gallery-demo.component';
import { ImageGalleryComponent } from './image-gallery/image-gallery.component';
import { FixedHeaderComponent } from './fixed-header/fixed-header.component';
import { FixedHeaderDirective } from './fixed-header/fixed-header.directive';
import { MatchMediaComponent } from './match-media/match-media.component';
import { MatchMediaDirective } from './match-media/match-media.directive';
import { MatchMediaService } from './match-media/match-media.service';

const appRoutes: Routes = [
  { path: 'home', component: AppHomeComponent },
  { path: 'browser-details', component: BrowserInfoComponent },
  { path: 'tooltip-demo', component: TooltipDemoComponent },
  { path: 'numeric-input', component: NumericInputComponent },
  { path: 'on-keycode', component: OnKeycodeComponent },
  { path: 'scroll-to', component: ScrollToComponent },
  { path: 'uppercase-input', component: UppercaseInputComponent },
  { path: 'alphanumeric-input', component: AlphanumericInputComponent },
  { path: 'currency-manipulator', component: CurrencyManipulatorComponent },
  { path: 'dynamic-form', component: DynamicFormComponent },
  { path: 'image-gallery', component: ImageGalleryDemoComponent },
  { path: 'fixed-header', component: FixedHeaderComponent },
  { path: 'match-media', component: MatchMediaComponent },
  { path: '', redirectTo: '/home', pathMatch: 'full' },
  { path: '**', component: AppHomeComponent }
];

@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
    RouterModule.forRoot(appRoutes)
  ],
  declarations: [
    AppComponent,
    AppHeaderComponent,
    AppHomeComponent,
    AppMenuComponent,
    BackToTopComponent,
    BrowserInfoComponent,
    AppTooltipComponent,
    TooltipDemoComponent,
    NumericInputComponent,
    NumericInputDirective,
    OnKeycodeComponent,
    OnKeycodeDirective,
    ScrollToComponent,
    ScrollToDirective,
    UppercaseInputComponent,
    UppercaseInputDirective,
    AlphanumericInputComponent,
    AlphanumericInputDirective,
    CurrencyManipulatorComponent,
    CurrencyManipulatorDirective,
    DynamicFormComponent,
    DynamicInputComponent,
    ImageGalleryDemoComponent,
    ImageGalleryComponent,
    FixedHeaderComponent,
    FixedHeaderDirective,
    MatchMediaComponent,
    MatchMediaDirective
  ],
  providers: [
    BrowserInfoService,
    MatchMediaService
  ],
  bootstrap: [ AppComponent ]
})
export class AppModule { }
