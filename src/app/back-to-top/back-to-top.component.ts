// This directive requires the smoothscroll-polyfill available at https://www.npmjs.com/package/smoothscroll-polyfill

import { Component, HostListener, Input } from '@angular/core';

@Component({
    selector: 'back-to-top',
    template: `<button type="button" class="back-to-top-btn" [class.show]="showButton" (click)="backToTop()"><span>Back to Top</span></button>`,
    styleUrls: ['./back-to-top.component.scss']
})
export class BackToTopComponent {
    @Input() showPosition: number = 100;
    
    public showButton: boolean = false;

    @HostListener('window:scroll', [])
    onWindowScroll() {
        this.showButton = (window.scrollY > this.showPosition) ? true : false;
    }
    
    backToTop() {
        window.scrollTo({top: 0, left: 0, behavior: 'smooth'});
    }
}