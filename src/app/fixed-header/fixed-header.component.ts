import { Component } from '@angular/core';

@Component({
  selector: 'fixed-header',
  templateUrl: './fixed-header.component.html',
  styleUrls: ['./fixed-header.component.scss']
})
export class FixedHeaderComponent { }