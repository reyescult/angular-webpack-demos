import { Directive, ElementRef, Renderer2, HostListener, Inject, OnInit } from '@angular/core';
import { DOCUMENT } from '@angular/platform-browser';

@Directive({
  selector: '[fixedHeader]'
})
export class FixedHeaderDirective implements OnInit {
  private body: HTMLElement;
  private docE: HTMLElement;
  private host: HTMLElement;
  private hostOffset: number;

  constructor(@Inject(DOCUMENT) private document: Document, private eRef: ElementRef, private renderer: Renderer2) {
    this.body = this.document.body;
    this.docE = this.document.documentElement;
    this.host = eRef.nativeElement;
  }

  ngOnInit() {
    this.hostOffset = this.host.offsetTop;
  }

  @HostListener('document:scroll', ['$event'])
  onscroll(e: any) {
    if (this.body.scrollTop > this.hostOffset || this.docE.scrollTop > this.hostOffset) {
      this.renderer.addClass(this.host, 'fixed');
    } else {
      this.renderer.removeClass(this.host, 'fixed');
    }
  }
}