// This directive requires the smoothscroll-polyfill available at https://www.npmjs.com/package/smoothscroll-polyfill

// Ex. <button type="button" scrollTo="header.main" scrollDelay="250"> * The scrollDelay attribute is optional.

import { Directive, HostListener, Input } from '@angular/core';

@Directive({
  selector: '[scrollTo]'
})
export class ScrollToDirective {
  @Input() scrollTo: string;
  @Input() scrollDelay: number = 0;

  @HostListener('click', ['$event'])
  onclick(e: any) {
    setTimeout(() => {
      if (this.scrollTo) {
        document.querySelector(this.scrollTo).scrollIntoView({ behavior: 'smooth' });
      }
    }, this.scrollDelay);
  }
}