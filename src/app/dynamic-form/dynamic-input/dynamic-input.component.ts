import { Component, EventEmitter, Input, Output, OnChanges } from '@angular/core';

@Component({
  selector: 'dynamic-input',
  templateUrl: './dynamic-input.component.html',
  styleUrls: ['./dynamic-input.component.scss']
})
export class DynamicInputComponent implements OnChanges {
  @Input() index: number;
  @Input() incomeSource: {};
  @Input() incomeTypes: {};
  @Output() onResetIncomeSource = new EventEmitter<number>();
  @Output() onRemoveIncomeSource = new EventEmitter<number>();

  public incomeIndex: number;

  ngOnChanges() {
    this.incomeIndex = this.index + 1;
  }
  
  public resetIncomeSource(index: number) {
    this.onResetIncomeSource.emit(index);
  }
  
  public removeIncomeSource(index: number) {
    this.onRemoveIncomeSource.emit(index);
  }
}