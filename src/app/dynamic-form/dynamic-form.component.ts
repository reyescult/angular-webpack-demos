import { Component } from '@angular/core';

@Component({
  selector: 'dynamic-form',
  templateUrl: './dynamic-form.component.html',
  styleUrls: ['./dynamic-form.component.scss']
})
export class DynamicFormComponent {
  public title: string = 'Dynamic Form Component';

  public incomeSources: Array<{}> = [
    {
      source: '',
      type: 'Please Select',
      gross: 0
    }
  ];

  public incomeTypes: Array<string> = ['Please Select', 'Salaried/Hourly', 'Self Employed'];

  public addIncomeSource() {
    this.incomeSources.push({
      source: '',
      type: 'Please Select',
      gross: 0
    });
  }

  onResetIncomeSource(index: number) {
    this.incomeSources.splice(index, 1, {
      source: '',
      type: 'Please Select',
      gross: 0
    });
  }

  onRemoveIncomeSource(index: number) {
    this.incomeSources.splice(index, 1);
  }
}
